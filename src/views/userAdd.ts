import { reactive, watch, ref } from 'vue'

export default function useAdd(formState: { num1: any; num2: any }) {

  const addNum = ref(0);

  watch(formState, (newVal) => {
    addFunc(newVal.num1, newVal.num2)
  })

  const addFunc = (num1: number, num2: number) => {
    return addNum.value = num1 + num2
  }

  return { addFunc, addNum }
}